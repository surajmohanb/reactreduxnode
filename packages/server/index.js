import express from 'express';
import path from 'path';
import morgan from 'morgan';
import bodyParser from 'body-parser';
const app = express();

var router = express.Router()

// middleware that is specific to this router
router.use((req, res, next) => {
  console.log('Time: ', Date.now())
  next()
})
app.use(morgan('combined'));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', router);
app.use(express.static("public")); 
router.get('/api', function (req, res) {
    res.send("http://www.mysite.com/");
});


app.listen(process.env.PORT || 3000, () => console.log("Listening to port 3000"));