import { connect } from 'react-redux'
import Home from '../../components/home'
 
import { getUsersInfo } from '../../redux/actions'
import { getUsers } from './selectors'

const mapStateToProps = (state, ownProps) => {
    return {
        users: getUsers(state, ownProps)
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUsers: () => dispatch(getUsersInfo())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)