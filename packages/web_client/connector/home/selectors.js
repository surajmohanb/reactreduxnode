import { createSelector } from 'reselect'

const getUsersData = (state) => state.users.get('users')
export const getUsers = createSelector(
    [ getUsersData ],
    (users) => users
  )