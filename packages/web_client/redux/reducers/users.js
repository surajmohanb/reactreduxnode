import { REQUEST_USER_DATA, RECEIVE_USER_DATA } from '../actions';
import { Map } from 'immutable'
const initPostState = Map({
    users: [],
    fetching: false,
    fetched: false,
    error: ''
})
const users = (state = initPostState, action) => {

    let updatedState = {};
    switch(action.type) {
        case REQUEST_USER_DATA: 
             state = state
                        .set('fetching', true)
                        .set('fetched', false)
            break;
        case RECEIVE_USER_DATA: 

            state = state
                        .set('fetching', false)
                        .set('fetched', true)
                        .set('users', action.payload)
            break;
    }
    return state;
}
export default users;